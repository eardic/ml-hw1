#ifndef _UTILS_H_
#define _UTILS_H_


#include <opencv2/core/core.hpp>


using namespace cv;


void read_data(Mat & readData, char * dataPath, char * fileNamePrefix, bool Debug = false);

void read_data_synthetic1(Mat & readData, int numItems, bool Debug = false);

void normalize_confusion_matrix(Mat & C);

void print_confusion_matrix(char * title, Mat & C);

void show_roc_curve(char * title, std::vector<cv::Point2f> & ROC);

void random_confusion_roc(Mat & C, std::vector<cv::Point2f> & ROC);

#endif // _UTILS_H_