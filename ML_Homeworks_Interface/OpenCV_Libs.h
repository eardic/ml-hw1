// OPENCV specific...
//
// Make sure that: $(OPENCVINCLUDE) environment variable is defined. 
//

#ifndef _OPENCV_LIBS_H_
#define _OPENCV_LIBS_H_

#pragma once

#define OPENCV_LIB_HOME "D:\\opencv\\build\\"

#ifdef _M_IX86
#define OPENCV_LIB_HOME_PROC OPENCV_LIB_HOME "x86\\vc12\\lib\\"
#else
#define OPENCV_LIB_HOME_PROC OPENCV_LIB_HOME "x64\\vc12\\lib\\"
#endif

#if _DEBUG
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_core248d.lib")
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_imgproc248d.lib")
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_highgui248d.lib")
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_ml248.lib")
#else
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_core248.lib")
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_imgproc248.lib")
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_highgui248.lib")
	#pragma comment(lib, OPENCV_LIB_HOME_PROC "opencv_ml248.lib")
#endif

#endif _OPENCV_LIBS_H_
