//-----------------------------------------------------------------------------------------------
//
// File: HWCR1.cpp
//
// Author: Yakup Genc.
//
// Copyright: December 10, 2012. Gebze. (c) Yakup Genc
//
// Explanation: Driver file for HW on Handwriten Character Recognition.
//
// History:
//   Dec 10, 2012: Created. Genc.
//
//-----------------------------------------------------------------------------------------------


// Homework should hand in only "HWCR_lastname_firstname_hw1.cpp". No change in function signatures is allowed.

// First output is expected to be a confusion matrix - for digits 0..9.

// Second output is expected to be ROC curves for each digit. Approximate the ROC curve to be a 10 by 20 matrix. 
// Each row contains true positive rates (y) you obtain for the 20 discrete bins for false positive rates (x). You
// can use 20 bins of size 1/20, meaning average the true positive rates for all the false positives rates in one bin.


#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <algorithm>// random_shuffle

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>

#include "Utils.hpp"

using namespace std;
using namespace cv;

// Forms a confusion matrix using actual and predicted labels
// Filled result matrix will be stored in "confMatrix"
void createConfusionMatrix(const Mat& actualLabels, const Mat& predictedLabels, Mat& confMatrix){
	confMatrix = confMatrix.zeros(10, 10, CV_32F);	
	for (int i = 0; i < actualLabels.rows ; ++i){
		int realLabel = (int)actualLabels.at<float>(i, 0);
		int prediction = (int)predictedLabels.at<float>(i, 0);
		confMatrix.at<float>(realLabel, prediction) += 1.0;
		//cout << realLabel << "-" << prediction << "-" << confMatrix.at<float>(realLabel, prediction) << endl;
	}
}

// Calculates TP,FP,FN,TN,TPR,FPR using confusion matrix.Then, calculates
// FPR and TPR values for each digit and calculates averages of TPR and FPR values
// Adds resulting averaged TPR and FPR to "ROC" vector
void createROCGraph(const Mat& confMatrix, std::vector<cv::Point2f> & ROC)
{
	//Foreach label
	double avgFpr=0, avgTpr=0;
	const int LABEL_COUNT = confMatrix.rows;
	for (int label = 0; label < LABEL_COUNT; ++label){
		
		// Find true positive
		double truePositive = confMatrix.at<float>(label, label);
		double positive = sum(confMatrix.col(label))[0];
				
		// Calculate True Positive Rate
		double tpr = truePositive / positive;
				
		// Count false positives
		double falsePositive = sum(confMatrix.row(label))[0] - truePositive;
	
		// Count false negatives
		double falseNegative = positive - truePositive;
		double trueNegative = sum(confMatrix)[0] - (truePositive+falsePositive+falseNegative);

		// Calculate False Positive Rate
		double fpr = falsePositive / (falsePositive + trueNegative);

		avgFpr += fpr / LABEL_COUNT;
		avgTpr += tpr / LABEL_COUNT;
	}
	ROC.push_back(Point2f((float)avgFpr, (float)avgTpr));
}

// Divides given "data" to "m" pieces and stores the pieces in "supsamples"
void subsample(const Mat& data, int m, vector<Mat>& subsamples)
{
	// Create vector containing 1d mat using given data
	vector<Mat> allRows;
	for (int i = 0; i < data.rows; ++i)
	{
		allRows.push_back(data.row(i));
	}	
	// Shuffle allRows vector, shuffles rows of given data matrix
	random_shuffle(allRows.begin(), allRows.end());
	// Create supsamples of given data matrix using allRows vector
	const int SAMPLE_SIZE = allRows.size() / m;
	subsamples.resize(m);
	for (int i = 0; i < m ; ++i)
	{
		subsamples[i] = Mat(0,data.cols,data.type());
		for (int j = 0; j < SAMPLE_SIZE; ++j){
			subsamples[i].push_back(allRows.back());
			allRows.pop_back();
		}
	}	
}

// Divide labels and data of given data and stored them in "labels" and "data" matrices
// Elements of output labels and data will be converted to "elementType "
void splitLabelAndData(const Mat& mergedData, Mat & labels, Mat &data, int elementType)
{
	labels = mergedData.col(0);	
	data = mergedData.colRange(1, mergedData.cols);
	labels.convertTo(labels, elementType);
	data.convertTo(data, elementType);
}

// Merges elements of "supsamples" vector except element at "testSetIndex"
// Merged matrices will be stored id "merged" matrix
void mergeSamples(vector<Mat>& subsamples, int testSetIndex, Mat& merged)
{
	for (unsigned int i = 0; i < subsamples.size(); ++i){
		if (i != testSetIndex){
			merged.push_back(subsamples[i]);
		}
	}
}

// Knn implementation using knn of opencv
// Evaluates knn classifier,training data and shows the results in "ConfusionMatrix" and "ROC"
void knn(Mat & trainingData, Mat & testingData, unsigned int k, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC)
{
	// Split labels and real data from training data	
	Mat trainLabels, trainData;
	splitLabelAndData(trainingData, trainLabels, trainData, CV_32F);

	// Train
	CvKNearest knn(trainData, trainLabels);

	// Split test label and test data
	Mat testLabels, testData;
	splitLabelAndData(testingData, testLabels, testData, CV_32F);

	Mat results;
	results = results.zeros(testingData.rows, 1, CV_32F);

	// Predict
	knn.find_nearest(testData, k, &results, 0, 0);

	// Create confusion matrix by evaluationg real and predicted labels
	createConfusionMatrix(testLabels, results, ConfusionMatrix);

	// Create ROC using confusion matrix
	createROCGraph(ConfusionMatrix, ROC);
}

// Svm implementation using svm of opencv
// Evaluates svm classifier,training data and shows the results in "ConfusionMatrix" and "ROC"
void svm(Mat & trainingData, Mat & testingData, CvSVMParams& params, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC)
{
	// Split labels and real data from training data	
	Mat trainLabels, trainData;
	splitLabelAndData(trainingData, trainLabels, trainData, CV_32F);

	// Train
	CvSVM SVM(trainData, trainLabels, Mat(), Mat(), params);

	// Split test label and test data
	Mat testLabels, testData;
	splitLabelAndData(testingData, testLabels, testData, CV_32F);

	Mat results;
	results = results.zeros(testingData.rows, 1, CV_32F);

	// Predict
	SVM.predict(testData, results);

	// Create confusion matrix
	createConfusionMatrix(testLabels, results, ConfusionMatrix);

	// Create ROC
	createROCGraph(ConfusionMatrix, ROC);
}

// k-NN algorithm - Euclidean distance
//   k: parameter for knn
//   m: m-fold cross validation
//   n: n repeats
void knn_e_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int k, unsigned int m, unsigned int n, Mat & ConfusionMatrix, vector<cv::Point2f> & ROC) {

	knn(trainingData, testingData, k, ConfusionMatrix, ROC);

	// Divide given training data to m equal set randomly
	vector<Mat> subsamples;
	subsample(trainingData, m, subsamples);
	// M fold cross validation, Select a test set and Use rest of he data as training set
	Mat totalConfMatrix(10,10,CV_32F,Scalar(0));
	for (unsigned int test = 0; test < n; ++test){
		Mat trainData;
		Mat confMatrix;
		//vector<cv::Point2f> rocCurve;
		mergeSamples(subsamples, test, trainData);
		knn(trainData, subsamples[test], k, confMatrix, ROC);
		totalConfMatrix += confMatrix;
	}
	totalConfMatrix /= n;
	print_confusion_matrix("EKNN CV", totalConfMatrix);

} // end knn_e_mfold_crossvalidation


// k-NN algorithm - Manhattan distance
//   k: parameter for knn
//   m: m-fold cross validation
//   n: n repeats
void knn_m_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int k, unsigned int m, unsigned int n, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC) {

	// OpenCV don't have manhattan distance metric for knn
	random_confusion_roc(ConfusionMatrix, ROC);	

} // end knn_m_mfold_crossvalidation

// linear SVM
//   m: m-fold cross validation
//   n: n repeats
void linear_svm_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int m, unsigned int n, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC) {

	//Configure Linear SVM
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::LINEAR;

	// Finds labels of test data and forms conf matrix and roc curve 
	svm(trainingData, testingData, params, ConfusionMatrix, ROC);

	// Divide given training data to m equal set randomly
	vector<Mat> subsamples;
	subsample(trainingData, m, subsamples);
	Mat totalConfMatrix(10, 10, CV_32F, Scalar(0));
	// M fold cross validation, Select a test set and Use rest of he data as training set
	for (unsigned int test = 0; test < n; ++test){
		Mat trainData;
		Mat confMatrix;
		// Select next test data in supsamples and create train data using other m-1 supsamples
		mergeSamples(subsamples, test, trainData);
		// Train, Predict, Evaluate
		svm(trainData, subsamples[test], params, confMatrix, ROC);
		totalConfMatrix += confMatrix;
	}
	totalConfMatrix /= n;
	print_confusion_matrix("LSVM CV", totalConfMatrix);

} // end linear_svm_mfold_crossvalidation


// polynomial power SVM - choose your parameters
//   m: m-fold cross validation
//   n: n repeats
void poly_svm_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int m, unsigned int n, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC) {

	//Configure Poly SVM
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::POLY;
	params.degree = 2;
	params.gamma = 1;
	params.coef0 = 1;

	svm(trainingData, testingData, params, ConfusionMatrix, ROC);

	// Divide given training data to m equal set randomly
	vector<Mat> subsamples;
	subsample(trainingData, m, subsamples);
	Mat totalConfMatrix(10, 10, CV_32F, Scalar(0));
	// M fold cross validation, Select a test set and Use rest of he data as training set
	for (unsigned int test = 0; test < n; ++test){
		Mat trainData;
		Mat confMatrix;
		// Select next test data in supsamples and create train data using other m-1 supsamples
		mergeSamples(subsamples, test, trainData);
		// Train, Predict, Evaluate
		svm(trainData, subsamples[test], params, confMatrix, ROC);
		totalConfMatrix += confMatrix;
	}
	totalConfMatrix /= n;
	print_confusion_matrix("PSVM CV", totalConfMatrix);

} // end poly_svm_mfold_crossvalidation
