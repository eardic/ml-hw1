//-----------------------------------------------------------------------------------------------
//
// File: Main.cpp
//
// Author: Yakup Genc.
//
// Copyright: March 2, 2014. Gebze. (c) Yakup Genc
//
// Explanation: Driver file for HWs on Handwritten Character Recognition.
//
// History:
//   Dec 10, 2012: Created. Genc.
//   Mar 2, 2014: Updated for HW1. Genc.
//
//-----------------------------------------------------------------------------------------------


#include "OpenCV_Libs.h"

#include "Utils.hpp"
#include "HWCR1.hpp"


#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>


using namespace std;
using namespace cv;



int test_hwcr1() {
	cv::Mat testingData;
	cv::Mat trainingData;

	cout << "Reading testing data...." << endl;
	read_data(testingData, "../../Data/DigitRecognition/","t10k", false);
	cout << "Reading training data...." << endl;
	read_data(trainingData, "../../Data/DigitRecognition/", "train", false);

	cv::Mat ConfusionMatrix1;
	std::vector<cv::Point2f> ROC1;
	knn_e_mfold_crossvalidation(trainingData, testingData, 5, 2, 2, ConfusionMatrix1, ROC1);
	print_confusion_matrix("5NNE", ConfusionMatrix1);
	show_roc_curve("5NNE", ROC1);

	cv::Mat ConfusionMatrix2;
	std::vector<cv::Point2f> ROC2;
	knn_m_mfold_crossvalidation(trainingData, testingData, 5, 2, 2, ConfusionMatrix2, ROC2);
	print_confusion_matrix("5NNM", ConfusionMatrix2);
	show_roc_curve("5NNM", ROC2);
	
	cv::Mat ConfusionMatrix3;
	std::vector<cv::Point2f> ROC3;
	linear_svm_mfold_crossvalidation(trainingData, testingData, 2, 2, ConfusionMatrix3, ROC3);
	print_confusion_matrix("L SVM", ConfusionMatrix3);
	show_roc_curve("L SVM", ROC3);
	
	cv::Mat ConfusionMatrix4;
	std::vector<cv::Point2f> ROC4;
	poly_svm_mfold_crossvalidation(trainingData, testingData, 2, 2, ConfusionMatrix4, ROC4);
	print_confusion_matrix("P SVM", ConfusionMatrix4);
	show_roc_curve("P SVM", ROC4);

	waitKey();
	return 0;
} // end test_hwcr1



int main(int argc, char *argv[], char *window_name) {
	test_hwcr1();
	return 0;
} // end main

