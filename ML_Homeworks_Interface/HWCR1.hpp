//-----------------------------------------------------------------------------------------------
//
// File: HWCR1.h
//
// Author: Yakup Genc.
//
// Copyright: December 10, 2012. Gebze. (c) Yakup Genc
//
// Explanation: Driver file for HW1 on Handwriten Character Recognition.
//
// History:
//   Dec 10, 2012: Created. Genc.
//
//-----------------------------------------------------------------------------------------------


// DO NOT MODIFY THIS FILE....



#ifndef _HWCR1_H_
#define _HWCR1_H_


#include <opencv2/core/core.hpp>


// k-NN algorithm - Euclidean distance
//   k: parameter for knn
//   m: m-fold cross validation
//   n: n repeats
void knn_e_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int k, unsigned int m, unsigned int n, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC);
// k-NN algorithm - Manhattan distance
//   k: parameter for knn
//   m: m-fold cross validation
//   n: n repeats
void knn_m_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int k, unsigned int m, unsigned int n, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC);
// linear SVM
//   m: m-fold cross validation
//   n: n repeats
void linear_svm_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int m, unsigned int n, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC);
// polynomial power SVM - choose your parameters
//   m: m-fold cross validation
//   n: n repeats
void poly_svm_mfold_crossvalidation(Mat & trainingData, Mat & testingData, unsigned int m, unsigned int n, Mat & ConfusionMatrix, std::vector<cv::Point2f> & ROC);


#endif