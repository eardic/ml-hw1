#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>


using namespace std;
using namespace cv;



// Tools to read the provided test data.
// Note that the data read has the following form
//    n rows - each row corresponds to a digit with the following format
//
//    digit_label image[0][0] image[0][1] ... image[0][c-1] image[1][0] ... image[r-1][0] ... image[r-1][c-1]
//
//    where r is the number of rows for each digit image and c is the number of columns.
//
// In this data set r=c=28

void read_data(Mat & readData, char * dataPath, char * fileNamePrefix, bool Debug = false) {
	unsigned long numItems;
	unsigned long intR;
	unsigned long numCols, numRows;

	// read the labels for the training data
	FILE * fin;
	char fileName[400]; 
	sprintf_s(fileName, "%s%s-labels.idx1-ubyte", dataPath, fileNamePrefix);
	fopen_s(&fin, fileName, "rb");
	fread_s(&intR, sizeof(int), sizeof(int), 1, fin);
	//cout << "Magic number: " << _byteswap_ulong(intR) << endl;
	fread_s(&intR, sizeof(int), sizeof(int), 1, fin);
	numItems = _byteswap_ulong(intR);
	cout << "Num items: " << numItems << endl;
	unsigned char * Labels = new unsigned char[numItems];
	fread_s(Labels, sizeof(unsigned char)*numItems, sizeof(unsigned char), numItems, fin);
	fclose(fin);
	if (Debug) {
		for (unsigned int i=0; i<numItems; i++) cout << (unsigned short)Labels[i] << " ";
		cout << endl;
	}

	// read the images for the training data
	sprintf_s(fileName, "%s%s-images.idx3-ubyte", dataPath, fileNamePrefix);
	fopen_s(&fin, fileName, "rb");
	fread_s(&intR, sizeof(int), sizeof(int), 1, fin);
	//cout << "Magic number: " << _byteswap_ulong(intR) << endl;
	fread_s(&intR, sizeof(int), sizeof(int), 1, fin);
	numItems = _byteswap_ulong(intR);
	cout << "Num items: " << numItems << endl;
	fread_s(&intR, sizeof(int), sizeof(int), 1, fin);
	numRows = _byteswap_ulong(intR);
	cout << "Num rows: " << numRows << endl;
	fread_s(&intR, sizeof(int), sizeof(int), 1, fin);
	numCols = _byteswap_ulong(intR);
	cout << "Num cols: " << numCols << endl;
	unsigned char * Images = new unsigned char[numItems*numCols*numRows];
	fread_s(Images, sizeof(unsigned char)*numItems*numCols*numRows, sizeof(unsigned char), numItems*numCols*numRows, fin);
	fclose(fin);
	if (Debug) {
		int numR = 20;
		int numC = 20;
		cv::Mat bImage = cv::Mat(numR*numRows, numC*numCols, CV_8U);
		for (unsigned int i=0; i<numItems; ) {
			for (int r=0; r<numR; r++) {
				for (int c=0; c<numC; c++) {
					Mat sImage = Mat(numRows, numCols, CV_8U, &Images[i*numRows*numCols]);
					sImage.copyTo(bImage(Rect(c*numCols, r*numRows, numRows, numCols)));
					i ++;
				}
			}
			cv::imshow("Character Recognition", bImage);
			cv::waitKey();
		}
		bImage.deallocate();
	}

	// compose the data
	readData = cv::Mat(numItems, 1 + numRows*numCols, CV_8U);
	for (unsigned int i=0; i<numItems; i++) {
		readData.at<unsigned char>(Point(0,i)) = Labels[i];
		for (unsigned int j=0; j<numRows*numCols; j++) {
			readData.at<unsigned char>(Point(1+j,i)) = Images[i*numRows*numCols + j];
		}
	}

	delete Labels;
	delete Images;
} // end read_data


// creates a synthetic data with 10 classes..
void read_data_synthetic1(Mat & readData, int numItems, bool Debug = false) {
	int numFeatures = 2;

	// compose the data
	readData = cv::Mat(numItems, 1 + numFeatures, CV_8U);
	for (int i=0; i<numItems; i++) {
		int c = rand() % 10;
		readData.at<unsigned char>(i,0) = (unsigned char)c;
		for (int j=0; j<numFeatures; j++) {
			readData.at<unsigned char>(i,j+1) = (unsigned char)(c+1)*18 + (rand() % 7);
		}
	}
} // end read_data_synthetic


// normalize the confusion matrix
void normalize_confusion_matrix(Mat & C) {
	for (int i=0;i<C.rows;i++) {
		float m = 0.00000000000000000000001f;
		for (int j=0;j<C.cols;j++) m += C.at<float>(i,j);
		for (int j=0;j<C.cols;j++) C.at<float>(i,j) /= m;
	}
} // end normalize_confusion_matrix

void printEvaluation(const Mat& confMatrix)
{
	//Foreach label
	double avgFpr = 0, avgTpr = 0, avgTp = 0, avgFp = 0, avgTn = 0, avgFn = 0;
	const int LABEL_COUNT = confMatrix.rows;
	for (int label = 0; label < LABEL_COUNT; ++label){
		// Find true positive
		double truePositive = confMatrix.at<float>(label, label);
		double positive = sum(confMatrix.col(label))[0];

		// Calculate True Positive Rate
		double tpr = truePositive / positive;

		// Count false positives
		double falsePositive = sum(confMatrix.row(label))[0] - truePositive;

		// Count false negatives
		double falseNegative = positive - truePositive;
		double trueNegative = sum(confMatrix)[0] - (truePositive + falsePositive + falseNegative);

		// Calculate False Positive Rate
		double fpr = falsePositive / (falsePositive + trueNegative);

		avgFpr += fpr / LABEL_COUNT;
		avgTpr += tpr / LABEL_COUNT;
		avgTp += truePositive / LABEL_COUNT;
		avgFp += falsePositive / LABEL_COUNT;
		avgTn += trueNegative / LABEL_COUNT;
		avgFn += falseNegative / LABEL_COUNT;
	}

	cout.precision(15);
	cout << ", TP:" << avgTp << ", FP:" << avgFp << ", TN:" << avgTn << ", FN:" << avgFn << endl;
	cout << "NPV : " << avgTn / (avgTn + avgFn) << endl;
	cout << "TPR : " << avgTpr << endl;
	cout << "FPR : " << avgFpr << endl;
	cout << "PRC : " << avgTp / (avgTp + avgFp) << endl;
	cout << "SPC : " << avgTn / (avgFp + avgTn) << endl;
	cout << "ACC : " << (avgTp + avgTn) / (avgTp + avgTn + avgFp + avgFn) << endl;
}

// print it...
void print_confusion_matrix(char * title, Mat & C) {
	cout << "Confusion matrix " << title << ":" << endl;
	for (int j=0; j<C.cols; j++) cout  << setw(6) << j; cout << endl;
	for (int i=0; i<C.rows; i++) {
		cout << i;
		for (int j=0; j<C.cols; j++) {
			cout << setw(6) << setiosflags(ios::fixed) << setprecision(0) << C.at<float>(i,j);
		}
		cout << endl;
	}
	//printEvaluation(C);
} // end print_confusion_matrix


// show the curve 
void show_roc_curve(char * title, std::vector<cv::Point2f> & ROC) {
	cout << "ROC " << title << ":" << endl;
	cv::Mat img(1000,1000,CV_8UC1);
	for (int x=0;x<img.cols;x++) {
		for (int y=0;y<img.rows;y++) {
			img.at<unsigned char>(x,y) = 0;
		}
	}
	cv::Point p1, p2;
	p1.x = (int)(img.cols*ROC.begin()->x);
	p1.y = img.rows-(int)(img.rows*ROC.begin()->y);
	for (std::vector<cv::Point2f>::iterator it=ROC.begin();it!=ROC.end();++it) {
		p2.x = (int)(img.cols*it->x);
		p2.y = img.rows-(int)(img.rows*it->y);
		cout << p1.x << " " << p1.y << " " << p2.x << " " << p2.y << endl;
		if (p2.x>=0 && p2.y>=0 && p2.x<img.cols && p2.y<img.rows) {
			cv::line(img, p1, p2, cv::Scalar(255));
			p1 = p2;
		}
	}
	cv::imshow(title,img);
} // end show_roc_curve


void random_confusion_roc(Mat & C, std::vector<cv::Point2f> & ROC) {
	// random confusion matrix
	C.create(10,10,CV_32F);
	for (int i=0;i<C.cols;i++) {
		for (int j=0;j<C.rows;j++) {
			C.at<float>(i,j) = rand() / (float)RAND_MAX;
		}
	}
	// random ROC 
	for (float x=0.0f;x<=1.0f; x+=0.05f) {
		float y = x;// + (1.0f-x)*(rand() / (float)RAND_MAX);
		cv::Point2f p(x,y); 
		ROC.push_back(p);
	}
} // end random_confusion_roc